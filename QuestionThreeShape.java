package com.ey.abstractkeyword;

abstract class Shape
{
	abstract void area();
}

class Circle extends Shape
{

	@Override
	void area() {
		System.out.println("Area of a circle is pi*r*r");
	}
}

class Square extends Shape
{

	@Override
	void area() {
		System.out.println("Area of a square is a*a");	
	}
}

class Rectangle extends Shape
{

	@Override
	void area() {
		System.out.println("Area of a rectangle is l*b");	
	}	
}
public class QuestionThreeShape {

	public static void main(String[] args) {
		
		Shape sh=new Circle();
		sh.area();
		Shape sh1=new Square();
		sh1.area();
		Shape sh2=new Rectangle();
		sh2.area();
	}

}


