package com.ey.abstractkeyword;

abstract class Drink
{
	abstract void taste(); 
}

class Sprite extends Drink
{
	@Override
	void taste() {
		System.out.println("Sprite Taste is Good");
	}
}

class Maaza extends Drink
{
	@Override
	void taste() { 
		System.out.println("Taste of Maaza is Good");	
	}
}
public class QuestionFiveDrink {

	public static void main(String[] args) {
		Drink d=new Sprite();
		d.taste();
		Drink d1=new Maaza();
		d1.taste(); 

	}

}

