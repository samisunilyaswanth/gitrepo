package com.ey.abstractkeyword;

abstract class Sim
{
	abstract void datapack();
}
class Airtel extends Sim
{
	@Override
	void datapack() {
		System.out.println("Datapack of Airtel");
	}
		
	void show()
	{
		System.out.println("calling by airtel");
	}
		
}
class Jio extends Sim
{
	@Override
	void datapack() {
		System.out.println("Datapck of Jio");
	}
	
}
public class QuestionOneSim {

	public static void main(String[] args) 
	{
		Sim s=new Airtel();
		s.datapack();
		Sim s1=new Jio();
		s1.datapack();
	}

}

