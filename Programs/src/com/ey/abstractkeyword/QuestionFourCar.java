package com.ey.abstractkeyword;
abstract class Car
{
	abstract void mileage();
}

class BmwCar extends Car
{

	@Override
	void mileage() {
		System.out.println("Bmw will give more mileage");
	}
}

class Ferari extends Car
{

	@Override
	void mileage() {
		System.out.println("Ferari will give more mileage");	
	}
	
}
public class QuestionFourCar { 

	public static void main(String[] args) {
		
		Car c=new BmwCar(); 
		c.mileage();
		Car c1=new Ferari();
		c1.mileage();
	}
}


