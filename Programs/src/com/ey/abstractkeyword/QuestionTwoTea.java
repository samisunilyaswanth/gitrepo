package com.ey.abstractkeyword;

abstract class Tea
{
	abstract void flavour();
	abstract void benefits();
}

class LemonTea extends Tea
{

	@Override
	void flavour() {
		System.out.println("Lemontea flavour is good");
	}

	@Override
	void benefits() {
		System.out.println("Lemontea Benefits");
		
	}	
}

class GreenTea extends Tea
{

	@Override
	void flavour() {
		System.out.println("Greentea flavour is good");
	}

	@Override
	void benefits() {
		System.out.println("Greentea Benefits");
		
	}	
}

class BlackTea extends Tea
{

	@Override
	void flavour() {
		System.out.println("Blacktea flavour is good");
	}

	@Override
	void benefits() {
		System.out.println("Blacktea Benefits");
		
	}	
}
public class QuestionTwoTea {

	public static void main(String[] args) {
		
		Tea t=new LemonTea();
		t.flavour();
		t.benefits();
		Tea t1=new GreenTea();
		t1.flavour();
		t1.benefits();
		Tea t2=new BlackTea();
		t2.flavour();
		t2.benefits();
	}

}

