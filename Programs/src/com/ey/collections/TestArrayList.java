package com.ey.collections;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

public class TestArrayList {

	public static void main(String[] args) {
		
		List<String>lst=new ArrayList<String>();  //creating a arraylist
		lst.add("Vishal");
		lst.add("Vishal2");
		lst.add("Vishal3");
		lst.add("Ravi");
		lst.add("Mahesh");
		System.out.println(lst); 
		int j=0;

		
//		while(j<5)
//		{                               //using while
//		System.out.println(lst.get(j));	
//		j++;
//		}
		
//		do {
//			System.out.println(lst.get(j));	//using do while
//			j++;
//		}while(j<5);
//		
		for(String a:lst)  //Enhanced for loop
		{
			System.out.println(a);
		}
		
		System.out.println(lst.contains("Vishal"));
//		
//		Iterator<String>it=lst.iterator();  //creating a iterator
//		while(it.hasNext())
//		{
//			System.out.println(it.next()); 
//		}
		
	}

}
