package com.ey.collections;

import java.util.LinkedList;
import java.util.List;

public class LinkedListOne {

	public static void main(String[] args) {


		List<String>ls=new LinkedList<String>();   
		ls.add("White");
		ls.add("Red");
		ls.add("Green");
		ls.add("Yellow");
		ls.add("Orange");
		System.out.println("LinkedList is: "+ls);
		
		ls.add("Blue");
		System.out.println("Updated LinkedList is: "+ls); 
		
		System.out.println(ls.contains("White")); 
		
		ls.add(1,"Saffron");
		System.out.println(ls); 
		
		ls.remove(1);
		System.out.println(ls); 

		LinkedList<Integer>l=new LinkedList<Integer>();   
		l.add(1);
		l.add(2);
		l.add(3);
		l.add(4);
		l.add(5);
		System.out.println("LinkedList integer is: "+l);
	}

}


