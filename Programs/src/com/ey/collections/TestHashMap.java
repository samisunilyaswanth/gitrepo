package com.ey.collections;

import java.util.HashMap;
import java.util.Iterator;

public class TestHashMap {

	public static void main(String[] args) {
		HashMap<Integer,String>hmOne=new HashMap<Integer,String>();
		hmOne.put(1,"Airtel"); 
		hmOne.put(2,"Jio"); 
		hmOne.put(3,"Idea"); 
		hmOne.put(4,"VodaFone"); 
		System.out.println(hmOne); 
		
		Iterator itrTwo=hmOne.keySet().iterator();
		while(itrTwo.hasNext())
		{
			Object oTwo=itrTwo.next();
			System.out.println(oTwo+" "+hmOne.get(oTwo)); 
		}
		

	}

}
