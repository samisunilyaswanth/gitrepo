package com.ey.collections;

import java.util.HashSet;
import java.util.Iterator;

public class TestHashSet {

	public static void main(String[] args) {
		
		HashSet<String>hOne=new HashSet<String>();  //creating a hashset 
		hOne.add("Pavan");
		hOne.add("Kiran");
		hOne.add("Rajesh");  //adding strings
		hOne.add("Satish");
		hOne.add("Rajesh");
		hOne.add("Karthik");
		System.out.println(hOne);
		
		System.out.println(hOne.contains("Pavan")); //checks if a string is there or not
		
		for(String b:hOne)  //Enhanced for loop
		{
			System.out.println(b);
		}
		
//		Iterator<String>itrTwo=hOne.iterator();  //using iterator
//		while(itrTwo.hasNext())
//		{
//			System.out.println(itrTwo.next());
//		}
//	
		hOne.add("Dharaneesh");
		System.out.println(hOne);
	}

}
