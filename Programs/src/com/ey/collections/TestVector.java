package com.ey.collections;

import java.util.Iterator;
import java.util.Vector;

public class TestVector {

	public static void main(String[] args) {
		
		Vector<String>list=new Vector<String>();   //creating a vector
		list.add("riyaj");
		list.add("mahesh");
		list.add("shirly");
		list.add("darshan");
		
		for(String a:list)  //Enhanced for loop
		{
			System.out.println(a); 
		}
		
		Iterator<String>itrOne=list.iterator();  //iterator
		while(itrOne.hasNext())
		{
			System.out.println(itrOne.next()); 
		}
		
		list.add("Sumanth");  //adding a list
		System.out.println(list); 
		
		System.out.println(list.get(4));   //display method
		
		list.remove(3);   //remove method
		System.out.println(list);
	}

}
