package com.ey.assignmentone;

public class EqualsAndHashCode {

	public static void main(String[] args) {
     //string using equals method
		String s1="Java";
		String s2="javatpoint";
		String s3="python";
		String s4="Python"; 
		System.out.println(s1.equals(s2));
		System.out.println(s3.equals(s4)); 
		
		//string using hashcode 
		System.out.println(s1.hashCode());
		System.out.println(s2.hashCode());
		System.out.println(s3.hashCode());
		System.out.println(s4.hashCode()); 
	}

}
