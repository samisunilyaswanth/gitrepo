package com.ey.assignmentone;

public class StringAndBuffer {

	public static void main(String[] args) {
		
		//program for string
		String s1="Java";  //creating a string
		char ch[]= {'e','c','l','i','p','s','e'};
		String s3=new String(ch);
		System.out.println(s1);  //printing string
		System.out.println(s3); 
		
		//program for stringbuffer
		StringBuffer sbOne=new StringBuffer();
		sbOne.append("javatpoint"); 
		System.out.println(sbOne); 
		sbOne.reverse();
		System.out.println(sbOne);
		System.out.println(sbOne.capacity());
	}

}
