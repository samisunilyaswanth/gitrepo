package com.ey.assignmentone;

public class AutoAndUnBoxing {

	public static void main(String[] args) {
		
		int a=60;
		Integer a1=new Integer(a);  //Autoboxing to UnBoxing
		System.out.println("AutoBoxing to UnBoxing: "+a1);
		
		Integer i=new Integer(70);
		int b=i;                     //UnBoxing to autoBoxing
		System.out.println("UnBoxing to AutoBoxing: "+b); 
		
		
	}

}
