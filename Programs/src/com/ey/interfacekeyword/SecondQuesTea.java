package com.ey.interfacekeyword;

interface Tea
{
	void flavour();
	void benefits(); 
}
class LemonTea implements Tea
{

	@Override
	public void flavour() {
		System.out.println("LemonTea Flavour is Good");
	}

	@Override
	public void benefits() {
		System.out.println("LemonTea benefits");
	}
}

class GreenTea implements Tea
{

	@Override
	public void flavour() {
		System.out.println("GreenTea Flavour is Good");
	}

	@Override
	public void benefits() {
		System.out.println("GreenTea benefits");
	}
}

class BlackTea implements Tea
{

	@Override
	public void flavour() {
		System.out.println("BlackTea Flavour is Good");
	}

	@Override
	public void benefits() {
		System.out.println("BlackTea benefits");
	}
}

public class SecondQuesTea {

	public static void main(String[] args) {
		
		Tea t=new LemonTea();
		t.flavour();
		t.benefits();
		Tea t1=new GreenTea();
		t1.flavour();
		t1.benefits();
		Tea t2=new BlackTea();
		t2.flavour();
		t2.benefits();
	}

}

