package com.ey.interfacekeyword;

interface Shape
{
	void area();
}

class Circle implements Shape
{

	@Override
	public void area() {
		System.out.println("Area of a circle is pi*r*r");
	}
}

class Square implements Shape
{

	@Override
	public void area() {
		System.out.println("Area of a square is a*a");	
	}
}

class Rectangle implements Shape 
{

	@Override
	public void area() {
		System.out.println("Area of a rectangle is l*b");	
	}	
}
public class ThirdQuesShape {

	public static void main(String[] args) {
		Shape s=new Circle();
		s.area();
		Shape s1=new Square();
		s1.area();
		Shape s2=new Rectangle();
		s2.area();
		
	}

}

