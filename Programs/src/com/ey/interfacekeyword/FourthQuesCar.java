package com.ey.interfacekeyword;

interface Car
{
	void mileage(); 
}

class BmwCar implements Car
{

	@Override
	public void mileage() {
		System.out.println("Bmw will give more mileage");
	}
}

class Ferari implements Car
{

	@Override
	public void mileage() { 
		System.out.println("Ferari will give more mileage");	
	}
}
public class FourthQuesCar {

	public static void main(String[] args) {
		 Car c=new BmwCar();
		 c.mileage();
		 Car c1=new Ferari();
		 c1.mileage();
	}

}

