package com.ey.interfacekeyword;

interface Drink
{
	void taste(); 
}

class Sprite implements Drink
{

	@Override
	public void taste() {
		System.out.println("Sprite Taste is Good");
	}
}

class Maaza implements Drink
{

	@Override
	public void taste() { 
		System.out.println("Taste of Maaza is Good");	
	}
}
public class FifthQuesDrink { 

	public static void main(String[] args) {
		Drink d=new Sprite();
		d.taste();
		Drink d1=new Maaza();
		d1.taste();

	}

}

