package com.ey.interfacekeyword;

interface Sim 
{
	void datapack();
}

class Airtel implements Sim
{
	@Override
	public void datapack() {
		System.out.println("Airtel Datapack");
		
	}
}

class Jio implements Sim
{
	@Override
	public void datapack() {
		System.out.println("Jio Datapack");
		
	}
}
public class FirstQuesSim {

	public static void main(String[] args) {
		Sim s=new Airtel();
		s.datapack();
		Sim s1=new Jio();
		s1.datapack();
	}

}

