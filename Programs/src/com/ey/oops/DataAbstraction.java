package com.ey.oops;

//Abstraction Using abstract 
//abstract class Tea //Creating a Abstract class Tea
//{
//	abstract void flavour();      //Creating a Abstract method
//	abstract void benefits();
//}
//
//class LemonTea extends Tea //Child class extends parent class method
//{
//
//	@Override          //Child class override parent class
//	void flavour() {
//		System.out.println("Lemontea flavour is good");
//	}
//
//	@Override
//	void benefits() {
//		System.out.println("Lemontea Benefits");
//		
//	}	
//}
//
//class GreenTea extends Tea
//{
//
//	@Override
//	void flavour() {
//		System.out.println("Greentea flavour is good");
//	}
//
//	@Override
//	void benefits() {
//		System.out.println("Greentea Benefits");
//		
//	}	
//}
//
//class BlackTea extends Tea
//{
//
//	@Override
//	void flavour() {
//		System.out.println("Blacktea flavour is good");
//	}
//
//	@Override
//	void benefits() {
//		System.out.println("Blacktea Benefits");
//		
//	}	
//}
//public class DataAbstraction { 
//
//	public static void main(String[] args) {
//		
//		Tea t=new LemonTea(); //creating a abstract class with child class method
//		t.flavour();
//		t.benefits();
//		Tea t1=new GreenTea();
//		t1.flavour();
//		t1.benefits();
//		Tea t2=new BlackTea();
//		t2.flavour();
//		t2.benefits();
//	}
//
//}

////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
///////////////////////////////////////////////////////

//Abstraction using interface

interface Drink   //creating an interface
{
	void taste(); //creating an interface method
}

class Sprite implements Drink   //child class implementing interface 
{

	@Override
	public void taste() {
		System.out.println("Sprite Taste is Good");
	}
}

class Maaza implements Drink    //child class implementing interface
{

	@Override
	public void taste() { 
		System.out.println("Taste of Maaza is Good");	
	}
}
public class DataAbstraction {

	public static void main(String[] args) {
		Drink d=new Sprite();   //creating an interface with child class
		d.taste();
		Drink d1=new Maaza();
		d1.taste();

	}

}

