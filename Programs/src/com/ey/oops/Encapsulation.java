package com.ey.oops;

class Mobile //Creating a Mobile Class
{
	String ram;
	String batteryCapacity;
	String simCard;
	String internalStorage;
	float cost;
	
	//Using getter() and setter() method
	public String getRam() {
		return ram;
	}
	public void setRam(String ram) {
		this.ram = ram;
	}
	public String getBatteryCapacity() {
		return batteryCapacity;
	}
	public void setBatteryCapacity(String batteryCapacity) {
		this.batteryCapacity = batteryCapacity;
	}
	public String getSimCard() {
		return simCard;
	}
	public void setSimCard(String simCard) {
		this.simCard = simCard;
	}
	public String getInternalStorage() {
		return internalStorage;
	}
	public void setInternalStorage(String internalStorage) {
		this.internalStorage = internalStorage;
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		this.cost = cost;
	}
	
}
public class Encapsulation {

	public static void main(String[] args) {
		Mobile m1=new Mobile();   //Creating a Mobile Class Object m1
		m1.setCost(250000);
		m1.setBatteryCapacity("4500mAh");
		m1.setInternalStorage("128GB");
		m1.setRam("8GB");
		m1.setSimCard("Jio");
		System.out.println(m1.getCost()+" "+m1.getBatteryCapacity()+" "+m1.getInternalStorage()+" "+m1.getRam()+" "+m1.getSimCard());
		
		Mobile m2=new Mobile();   //Creating a Mobile Class Object m1
		m2.setCost(200000);
		m2.setBatteryCapacity("5000mAh");
		m2.setInternalStorage("64GB");
		m2.setRam("6GB"); 
		m2.setSimCard("Airtel");
		System.out.println(m2.getCost()+" "+m2.getBatteryCapacity()+" "+m2.getInternalStorage()+" "+m2.getRam()+" "+m2.getSimCard());
		

	}

}
