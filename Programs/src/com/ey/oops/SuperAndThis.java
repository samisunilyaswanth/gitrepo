package com.ey.oops;
//Using Super  
//class MobileOne //creating mobile class
//{
//	String sim="Jio";
//}
//
//class Samsung extends MobileOne //child class extending parent class
//{
//	String sim="Airtel";
//	void print()
//	{
//		System.out.println(sim);  //prints child class variable
//		System.out.println(super.sim); //printing parent class variable
//	}
//}
//
//public class SuperAndThis {
//
//	public static void main(String[] args) {
//		
//		Samsung s1=new Samsung(); //creating child class object
//		s1.print(); 
//	}
//}


//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
//using this
class Employee2      //creating a employee class
{
	int id3;
	String name3;
	String address1;

Employee2(int id3,String name3,String address1) //parameterized constructor with this 
{
	this.id3=id3;
	this.name3=name3;
	this.address1=address1;
}

void print1()  //method
{
	System.out.println(id3+" "+name3+" "+address1);
}
}

public class SuperAndThis {

	public static void main(String[] args) {
		
		Employee2 e2=new Employee2(57,"Kiran","Tirupathi");
		Employee2 e3=new Employee2(43,"Pavan","Guntur"); 
		e2.print1();
		e3.print1();
	}
}
