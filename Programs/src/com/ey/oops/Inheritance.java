package com.ey.oops;

//Multilevel Inheritance

//class Sim                 //Creating a Parent Class
//{
//	void data()
//	{
//		System.out.println("Datpack depends on the sim"); 
//	}
//}
//
//class Airtel extends Sim   //Creating a child class Airtel extending Parent class
//{
//	void datapack()
//	{
//		System.out.println("Airtel - Datapack of 1.5 GB per day");
//	}
//}
//
//class Jio extends Airtel    //Creating a child class Jio extending another parent class
//{
//	void datapack1()
//	{
//		System.out.println(" Jio - Datpack of 2 GB per day");
//	}
//}
//
//public class MultiLevel {
//
//	public static void main(String[] args) {
//		
//		Jio j1=new Jio();  //creating a Jio class object 
//		j1.data();
//		j1.datapack();
//		j1.datapack1();
//	}
//
//}


////////////////////////////////////////////////////////
///////////////////////////////////////////////////////
///////////////////////////////////////////////////////

 //Single level Inheritance

//class Car //Creating a 
//{
//	void color()
//	{
//		System.out.println("Car has more colors to buy");
//	}
//}
//
//class BmwCar extends Car
//{
//	void price()
//	{
//		System.out.println("Cost of BmwCar Price is 50 Lakhs");
//	}
//}
//class MultiLevel 
//{
//
//	public static void main(String[] args) {
//		
//		BmwCar b1=new BmwCar();
//		b1.color();
//		b1.price();
//	}
//
//}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////

//Hierarchial Inheritance

class Bike
{
	void price()
	{
		System.out.println("Based on the Specification of Bike, Cost will be decided");
	}
} 

class Activa extends Bike
{
	void mileage()
	{
		System.out.println("Activa - A mileage of 40 kmpl");
	}
}

class Honda extends Bike
{
	void mileage1()
	{
		System.out.println("Honda - A mileage of 45 kmpl"); 
	}
}

public class Inheritance { 

	public static void main(String[] args) {
		
		Activa a1=new Activa();
		a1.price();
		a1.mileage();
		Honda h1=new Honda();
		h1.price();
		h1.mileage1();
	}
}

