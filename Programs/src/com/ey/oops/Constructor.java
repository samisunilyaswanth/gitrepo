package com.ey.oops;
//Default Constructor
//public class Constructor {
//
//	Constructor()   //Creating Default Constructor
//	{
//		System.out.println("This is my constructor class");
//	}
//	public static void main(String[] args) {
//		
//		Constructor c1=new Constructor();   //calling the Default constructor method
//	}
//
//}


//////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////

//Parameterized Constructor
public class Constructor     //creating a class
{
	int id1;   //data members or instance variables
	String name1;
	Constructor(int id2,String name2)   //parameterized constructor
	{
		id1=id2;
		name1=name2;
		
	}
	void print() //method
	{
		System.out.println(id1+" "+name1); 
	}
	public static void main(String [] args)
	{
		 Constructor c2=new Constructor(45,"Prudhvi"); 
		 Constructor c3=new Constructor(56,"Lokesh");
		 c2.print();
		 c3.print();
	}
}