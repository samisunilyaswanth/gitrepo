package com.ey.oops;
//Method Overloading
//class Addition  //Creating a class Addition
//{
//	static int sum(int a, int b)
//	{
//		return a+b;
//	}
//	 //Creating two methods with same name but arguments are different
//	
//	static double sum(double c, double d)
//	{
//		return c+d; 
//	}
//	
//	
//}
//public class Polymorphism {
//
//	public static void main(String[] args) {
//		
//		System.out.println(Addition.sum(15,30)); 
//		System.out.println(Addition.sum(20.0,30.0)); 
//		
//	}
//
//}

//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
class Vehicle  //Creating a new class vehicle
{
	void show()
	{
		System.out.println("Bike is running properly");
	}
}

class Car extends Vehicle //child class extending parent class
{
	void show()
	{
		System.out.println("Bike is running properly");
	}
}

public class Polymorphism {
	
	public static void main(String [] args)
	{
		Car b1=new Car(); //creating child class object
		b1.show();
	}
}
