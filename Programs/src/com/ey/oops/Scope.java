package com.ey.oops;

public class Scope {
	
	private int price; 
	public String nameOne;
	public Scope(String nameTwo)
	{
		nameOne=nameTwo;
	}
	public void setPrice(int priceOne)
	{
		price=priceOne;
	}
	
	public void printShow()
	{
		System.out.println("Product Name: "+nameOne);
		System.out.println("price :"+price);
	}

	public static void main(String[] args) {
		
		Scope s1=new Scope("Dell");
		s1.setPrice(60000); 
		s1.printShow();
	}

}
