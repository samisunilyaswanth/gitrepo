package com.ey.oops;
//Class and Object through reference
class Student //Creating a Student Class
{
	int id;
	String name;
	String address;
}

public class Object {

	public static void main(String[] args) {
		
		Student s1=new Student(); //Creating a Object
		s1.id=4;
		s1.name="Yaswanth";     //Referencing a value
		s1.address="Bangalore";
		System.out.println(s1.id+" "+s1.name+" "+s1.address);
		Student s2=new Student();
		s2.id=2;
		s2.name="Lokesh";
		s2.address="Chennai";
		System.out.println(s2.id+" "+s2.name+" "+s2.address); 


	}

}
